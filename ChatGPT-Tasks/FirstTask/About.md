**SORU**:çeşitli kaynaklardan veri toplamak istiyorum pythonın Luigi kütüphanesini kullanmaya yeni başlayacak birine alıştırma yapması için bir ödev ver

**Alıştırma**: Veri Toplama Pipeline'ı

1. Adım: Veri indirme task'ı yazın
Başlangıç olarak, veri indirme task'ını yazın. Bu task, veri indirme işlemi için gerekli olan URL'yi alacak ve veriyi indirecek.
Bu task, luigi.Task sınıfından türetilmeli ve run metodu içinde veri indirme işlemi gerçekleştirilmeli. Örneğin, requests kütüphanesi kullanarak veriyi indirebilirsiniz.
Ayrıca, task'ın output metodu tanımlanmalı ve bu metod, indirilen verinin kaydedileceği dosya adını döndürmelidir.
2. Adım: Veri temizleme task'ı yazın
Veri indirme task'ının çıktısı olarak, veri temizleme task'ı yazın. Bu task, indirilen veriyi temizleyecek ve temiz veriyi kaydedecek.
Bu task, luigi.Task sınıfından türetilmeli ve run metodu içinde veri temizleme işlemi gerçekleştirilmeli. Örneğin, pandas kütüphanesi kullanarak veriyi okuyabilir ve dropna() metodu kullanarak boş değerleri silebilirsiniz.
Ayrıca, task'ın requires metodu tanımlanmalı ve bu metod, veri indirme task'ının çıktısı olarak döndürülen dosya adını almalıdır.
3. Adım: Veri analiz task'ı yazın
Veri temizleme task'ının çıktısı olarak, veri analiz task'ı yazın. Bu task, temiz veriyi analiz edecek ve analiz sonuçlarını kaydedecek.
Bu task, luigi.Task sınıfından türetilmeli ve run metodu içinde veri analiz işlemi gerçekleştirilmeli. Örneğin, pandas kütüphanesi kullanarak veriyi okuyabilir ve describe() metodu kullanarak verinin özet bilgisini elde edebilirsiniz.
Ayrıca, task'ın requires metodu tanımlanmalı ve bu metod, veri temizleme task'ının çıktısı olarak döndürülmeli
4. Adım: Pipeline'ı çalıştırın
Adım 1, 2 ve 3'de yazdığınız task'ları kullanarak, veri indirme, veri temizleme ve veri analiz işlemlerini gerçekleştiren bir pipeline oluşturun.
Bu pipeline'ı luigi.build() fonksiyonu kullanarak çalıştırın. Örnek kod aşağıdaki gibi olabilir:
```
if __name__ == '__main__':
    luigi.build([PrintHello()],local_scheduler=True)
```
Bu pipeline'ı çalıştırdıktan sonra, veri indirme, veri temizleme ve veri analiz işlemleri gerçekleştirilecek ve sonuçlar kaydedilecektir.
5. Adım: Sonuçları kontrol edin
Veri indirme, veri temizleme ve veri analiz işlemleri gerçekleştirildikten sonra, oluşan sonuç dosyalarını kontrol edin.
Veri indirme task'ı tarafından indirilen veri, veri temizleme task'ı tarafından temizlenmiş veri ve veri analiz task'ı tarafından analiz edilmiş veri dosyalarını kontrol edin.
Eğer dosyalar oluşmuş ve içerikleri doğru ise, pipeline başarıyla çalışmış demektir.
### Bu alıştırma sadece veri indirme, veri temizleme ve veri analiz işlemlerini gerçekleştiren bir pipeline içermektedir. Gerçek bir pipeline için daha fazla task'lar ekleyebilirsiniz, örnek; verilerin kaydedilmesi, verilerin raporlama vb.