import os
import luigi
import pandas as pd
import requests as req
from bs4 import BeautifulSoup

class DownloadData(luigi.Task):

    def run(self):
        site = req.get("http://www.gutenberg.org/browse/scores/top").text
        with self.output().open("w") as f:
            f.write(site)

    def output(self):
        return luigi.LocalTarget("raw_data.txt")

    def complete(self):
        return os.path.exists(self.output().path)

class PrePData(luigi.Task):

    def requires(self):
        return DownloadData()

    def run(self):
        data = self.requires().output()
        bs4ed_data=BeautifulSoup(data.open(),"html.parser").text
        with self.output().open("w") as f:
            f.write(bs4ed_data)

    def output(self):
        return luigi.LocalTarget("new_data.txt")

    def complete(self):
        return os.path.exists(self.output().path)

class AnalysisData(luigi.Task):

    def requires(self):
        return PrePData()

    def run(self):
        data = self.requires().output()
        # nlp analiz yöntemleri
        with self.output().open("w") as f:
            f.write("nlp analiz yöntemleri")

    def output(self):
        return luigi.LocalTarget("analysis_results.txt")

class RunAllTasks(luigi.WrapperTask):
    def requires(self):
        return [DownloadData(),PrePData(),AnalysisData()]
